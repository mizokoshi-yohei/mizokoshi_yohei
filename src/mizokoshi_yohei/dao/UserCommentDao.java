package mizokoshi_yohei.dao;

import static mizokoshi_yohei.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import mizokoshi_yohei.beans.UserComment;
import mizokoshi_yohei.exception.SQLRuntimeException;

public class UserCommentDao {

    public List<UserComment> getUserComment(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("comments.id as id, ");
            sql.append("comments.comment as comment, ");
            sql.append("comments.user_id as user_id, ");
            sql.append("users.name as name, ");
            sql.append("comments.created_date as created_date, ");
            sql.append("comments.message_id as message_id ");            
            sql.append("FROM comments ");
            sql.append("INNER JOIN users ");
            sql.append("ON comments.user_id = users.id ");
            sql.append("ORDER BY created_date DESC limit " + num);
            
            
           




            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserComment> ret = toUserCommentList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserComment> toUserCommentList(ResultSet rs)
            throws SQLException {

        List<UserComment> ret = new ArrayList<UserComment>();
        try {
            while (rs.next()) {
                String comment = rs.getString("comment");
                String name = rs.getString("name");
                int id = rs.getInt("id");
                int user_id = rs.getInt("user_id");
                int message_id = rs.getInt("message_id");
                Timestamp createdDate = rs.getTimestamp("created_date");

                UserComment comments = new UserComment();
                comments.setComment(comment);
                comments.setName(name);
                comments.setId(id);
                comments.setUserId(user_id);
                comments.setMessage_id(message_id);
                comments.setCreated_date(createdDate);

                ret.add(comments);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}