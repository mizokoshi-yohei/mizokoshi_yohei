package mizokoshi_yohei.dao;

import static mizokoshi_yohei.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import mizokoshi_yohei.beans.Comment;
import mizokoshi_yohei.exception.SQLRuntimeException;

public class CommentDao {

    public void insert(Connection connection, Comment comment) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO comments ( ");
            sql.append("comment");
            sql.append(",id");
            sql.append(",user_id");
            sql.append(",message_id");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append(" ?"); // comment
            sql.append(", ?"); // id
            sql.append(", ?"); // user_id
            sql.append(", ?");
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, comment.getComment());
            ps.setInt(2, comment.getId());
            ps.setInt(3, comment.getUser_id());
            ps.setInt(4, comment.getMessage_id());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

}
