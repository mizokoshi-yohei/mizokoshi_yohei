package mizokoshi_yohei.dao;

import static mizokoshi_yohei.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import mizokoshi_yohei.beans.Message;
import mizokoshi_yohei.exception.SQLRuntimeException;

public class UserMessageDao {

    public List<Message> getUserMessages(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("messages.id as id, ");
            sql.append("messages.text as text, ");
            sql.append("messages.user_id as user_id, ");
            sql.append("messages.title as title, ");
            sql.append("messages.category as category, ");
            sql.append("users.name as name, ");
            sql.append("messages.created_date as created_date ");
            sql.append("FROM messages ");
            sql.append("INNER JOIN users ");
            sql.append("ON messages.user_id = users.id ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<Message> ret = toUserMessageList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<Message> toUserMessageList(ResultSet rs)
            throws SQLException {

        List<Message> ret = new ArrayList<Message>();
        try {
            while (rs.next()) {
                String category = rs.getString("category");
                String name = rs.getString("name");
                int id = rs.getInt("id");
                int user_id = rs.getInt("user_id");
                String title = rs.getString("title");
                String text = rs.getString("text");
                Timestamp createdDate = rs.getTimestamp("created_date");

                Message message = new Message();
                message.setCategory(category);
                message.setName(name);
                message.setId(id);
                message.setUserId(user_id);
                message.setTitle(title);
                message.setText(text);
                message.setCreatedDate(createdDate);

                ret.add(message);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}