package mizokoshi_yohei.service;

import static mizokoshi_yohei.utils.CloseableUtil.*;
import static mizokoshi_yohei.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import mizokoshi_yohei.beans.Comment;
import mizokoshi_yohei.beans.UserComment;
import mizokoshi_yohei.dao.CommentDao;
import mizokoshi_yohei.dao.UserCommentDao;


public class CommentService {

    public void register(Comment comment) {

        Connection connection = null;
        try {
            connection = getConnection();

            CommentDao commentDao = new CommentDao();
            commentDao.insert(connection, comment);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    private static final int LIMIT_NUM = 500;

    public List<UserComment> getComment() {

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		UserCommentDao commentDao = new UserCommentDao();
    		List<UserComment> ret = commentDao.getUserComment(connection, LIMIT_NUM);

    		commit(connection);

    		return ret;
    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}
    }

}