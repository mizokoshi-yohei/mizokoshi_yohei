package mizokoshi_yohei.service;

import static mizokoshi_yohei.utils.CloseableUtil.*;
import static mizokoshi_yohei.utils.DBUtil.*;

import java.sql.Connection;

import mizokoshi_yohei.beans.User;
import mizokoshi_yohei.dao.UserDao;
import mizokoshi_yohei.utils.CipherUtil;

public class LoginService {
	public User login(String loginId, String password) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			String encPassword = CipherUtil.encrypt(password);
			User user = userDao.getUser(connection, loginId, encPassword);

			commit(connection);

			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}

