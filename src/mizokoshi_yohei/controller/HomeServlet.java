package mizokoshi_yohei.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mizokoshi_yohei.beans.Message;
import mizokoshi_yohei.beans.User;
import mizokoshi_yohei.beans.UserComment;
import mizokoshi_yohei.service.CommentService;
import mizokoshi_yohei.service.MessageService;


@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        User user = (User) request.getSession().getAttribute("loginUser");
        boolean isShowMessageForm;
        if (user != null) {
            isShowMessageForm = true;
        } else {
            isShowMessageForm = false;
        }

        List<Message> messages = new MessageService().getMessage();
        List<UserComment> comments = new CommentService().getComment();

        //System.out.println(messages.getUserId());
        request.setAttribute("messages", messages);
        request.setAttribute("comments", comments);
        request.setAttribute("isShowMessageForm", isShowMessageForm);
        


        request.getRequestDispatcher("/Home.jsp").forward(request, response);
    
    
    }
}