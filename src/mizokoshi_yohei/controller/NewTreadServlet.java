package mizokoshi_yohei.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import mizokoshi_yohei.beans.Message;
import mizokoshi_yohei.beans.User;
import mizokoshi_yohei.service.MessageService;

@WebServlet(urlPatterns = { "/newTread" })
public class NewTreadServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("NewTread.jsp").forward(request, response);
    }
    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        List<String> messages = new ArrayList<String>();

        if (isValid(request, messages) == true) {

            User user = (User) session.getAttribute("loginUser");

            Message message = new Message();
            message.setTitle(request.getParameter("title"));
            message.setText(request.getParameter("text"));
            message.setCategory(request.getParameter("category"));
            message.setUserId(user.getId());

            new MessageService().register(message);

            response.sendRedirect("Home");
        } else {
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("newTread");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {

        String title = request.getParameter("title");
        String text = request.getParameter("text");
        String category = request.getParameter("category");

        if (StringUtils.isEmpty(title) == true) {
            messages.add("件名を入力してください");
        }
        if (StringUtils.isEmpty(title) != true && title.length() > 30) {
        	messages.add("件名は30文字以下で入力してください");
        }
        if (StringUtils.isEmpty(text) == true) {
            messages.add("本文を入力してください");
        }
        if (StringUtils.isEmpty(text) != true && 1000 < text.length()) {
            messages.add("本文は1000文字以下で入力してください");
        }
        if (StringUtils.isEmpty(category) == true) {
            messages.add("カテゴリーを入力してください");
        }
        if (StringUtils.isEmpty(title) != true && category.length() > 10) {
            messages.add("カテゴリー名は10字以下で入力してください");
        }
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}