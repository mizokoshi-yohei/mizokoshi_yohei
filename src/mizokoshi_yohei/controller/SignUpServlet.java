package mizokoshi_yohei.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import mizokoshi_yohei.beans.User;
import mizokoshi_yohei.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("SignUp.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<String> messages = new ArrayList<String>();

        HttpSession session = request.getSession();
        if (isValid(request, messages) == true) {

            User user = new User();
            user.setName(request.getParameter("name"));
            user.setLoginId(request.getParameter("loginId"));
            user.setPassword(request.getParameter("password"));
            user.setBrunches(request.getParameter("brunches"));
            user.setPosition(request.getParameter("position"));

            new UserService().register(user);

            response.sendRedirect("usermanagement");
        } else {
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("signup");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {
        String name = request.getParameter("name");
        String loginId = request.getParameter("loginId");
        String password = request.getParameter("password");
        String brunches = request.getParameter("brunches");
        String position = request.getParameter("position");
        
        

        if (StringUtils.isEmpty(name) == true) {
            messages.add("名前を入力してください");
        }
        if (StringUtils.isEmpty(name) != true && !(name.length() <= 10)) {
        	messages.add("名称は10文字以下にしてください");
        }
        if (StringUtils.isEmpty(loginId) == true) {
            messages.add("ログインIDを入力してください");
        }
        if (StringUtils.isEmpty(loginId) != true && !(loginId.matches("^[0-9a-z]+$") && loginId.length() >= 6 && loginId.length() <= 20)) {
        	messages.add("半角英数字6文字以上20文字以内で入力してください");
        }
        if (StringUtils.isEmpty(password) == true) {
            messages.add("パスワードを入力してください");
        }
        if (StringUtils.isEmpty(password) != true && !(password.length() >= 6 && password.length() <= 20)) {
        	messages.add("記号を含む全ての半角文字で6文字以上20文字以下で入力してください");
        }
        if (StringUtils.isEmpty(brunches) == true) {
            messages.add("支店名を入力してください");
        }
        if (StringUtils.isEmpty(position) == true) {
            messages.add("部署・役職を入力してください");
        }
        // TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}