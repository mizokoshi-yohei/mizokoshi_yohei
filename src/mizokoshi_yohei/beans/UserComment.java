package mizokoshi_yohei.beans;

import java.util.Date;

public class UserComment {
	
	private int id;
    private String comment;
    private String name;
    private int user_Id;
    private int Message_id;
    private Date created_date;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
		
	}
	public int getUserId() {
		return user_Id;
	}
	public void setUserId(int user_Id) {
		this.user_Id = user_Id;
	}
	public int getMessage_id() {
		return Message_id;
	}
	public void setMessage_id(int Message_id) {
		this.Message_id = Message_id;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}


}
