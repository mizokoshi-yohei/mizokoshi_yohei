package mizokoshi_yohei.beans;

import java.io.Serializable;
import java.util.Date;


public class Comment implements Serializable {
    private static final long serialVersionUID = 1L;

    
    private int id;
    private String comment;
    private String name;
    private int user_id;
    private int Message_id;
    private Date createdDate;
    private Date updatedDate;
   

    public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public int getMessage_id() {
		return Message_id;
	}
	public void setMessage_id(int Message_id) {
		this.Message_id = Message_id;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

}