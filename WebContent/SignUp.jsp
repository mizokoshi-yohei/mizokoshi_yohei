<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>ユーザー登録</title>
    <link href="./css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="main-contents">
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>
            <form action="signup" method="post">
                <br /> <label for="name">名前</label> <input name="name" id="name" />
                <br /> <label for="loginId">ログインID</label> <input name="loginId" id="loginId" /> 
                <br /> <label for="password">パスワード</label> <input name="password" type="password" id="password" /> 
                <br /> <label for="brunches">支店</label> <input type="number" min="1" name="brunches" id="brunches" />
                <br /> <label for="position">部署・役職</label> <input type="number" min="1" name="position" id="position" />
                
                
                
                <br /> <input type="submit" value="登録" /> <br /> <a href="./">戻る</a>
            </form>
            <div class="copyright">Copyright(c)mizokoshi_yohei</div>
        </div>
    </body>
</html>