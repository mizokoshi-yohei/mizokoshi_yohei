<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>HOME</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="main-contents">
		
		<form method="post" action="SearchCategory">
        <div>検索したいカテゴリーを入力してください</div>
        <div>
            <input type="text" name="name" maxlength="20" size="20">
        </div>
         <input type="submit" value="検索">
    </form>
			
			<div class="messages">
			    <c:forEach items="${messages}" var="message">
			    
			            <div class="message">
			                <div class="account-name">
			                    <span class="name"><c:out value="${message.name}" /></span>
			                </div>
			                <div class="title"><c:out value="${message.title}" /></div>
			                <div class="text"><c:out value="${message.text}" /></div>
			                <div class="category"><c:out value="${message.category }" /></div>
			                <div class="date"><fmt:formatDate value="${message.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
			                
			            </div>
			        
 			        <c:if test="${loginUser.id == message.userId}">
 			        <form action="messageDelete" method="post">
			        <input type="submit" value="削除">
			        
			        </form>
			        </c:if>
			        
			        <c:forEach items="${comments}" var="comment" >
			        
			        <c:if test="${message.id == comment.message_id }">
			             <div class="comment">
			                 <div class="comment"><c:out value="${comment.comment}" /></div>
			                 <span class="name"><c:out value="${comment.name}" /></span>
			                 <div class="date"><fmt:formatDate value="${comment.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
			             </div>
			        </c:if>
			     </c:forEach>
			       
			     
		<div class="form-area">
				<c:if test="${ isShowMessageForm }">
					<form action="newComment" method="post">
						コメント<br />
						<textarea name="comment" cols="100" rows="5" class="tweet-box"></textarea>
						<input  type="hidden" name="message_id"value="${message.id }"/>>
						<br />
						<input type="submit" value="コメントする">
					</form>
				</c:if>
			</div>
			
			<c:if test="${ not empty errorComment }">
			<div class="errorComment">
				<ul>
					<c:forEach items="${errorComment}" var="comments">
						<li><c:out value="${comments}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorComment" scope="session" />
		</c:if>
		</c:forEach>
			                <div class="date"><fmt:formatDate value="${message.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
			            </div>
			    
			</div>
			
			<a href="newTread">新規投稿</a>
			<div class="copylight"> Copyright(c)mizokoshi_yohei</div>
		
	</body>
</html>